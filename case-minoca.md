# A challenging case of myocardial infarction with non-obstructive coronary arteries (MINOCA)

Authors: A. Skrobisz, M. Demkow, A. Witkowski, A. Januszewicz, J. Stępińska  

## Introduction

According to _2017 ESC Guidelines for the management of acute myocardial infarction in patients presenting with ST-segment elevation_ (Guidelines):

> A sizeable proportion of STEMI patients do not present significant coronary artery stenosis on urgent angiography. It is important to perform additional diagnostic tests in these patients to identify the aetiology and tailor appropriate therapy, which may be different from typical STEMI.

Subsequently a new concept of myocardial infarction with non-obstructive coronary arteries (MINOCA) is introduced, with the following diagnostic criteria to be applied immediately upon coronary angiography in a patient presenting with features consistent with an AMI:

1. Universal acute myocardial infarction (AMI) criteria.
2. Non-obstructive coronary arteries on angiography, defined as no coronary artery stenosis ≥50% in any potential infarct-related artery (IRA).
3. No clinically overt specific cause for the acute presentation.

MINOCA is a serious diagnosis, as the Guidelines establish 1 year mortality of about 3.5%.(citation)

## Case report

A 52-year-old man was admitted after resuscitation due to ventrical fibrillation (VF). The patient was unconscious, in cardiogenic shock, with LVEF of 40%. ECG showed negative T-wave in leads V1-V6. Intravascular therapeutic hypothermia was initiated. 

History included anterior ST elevation myocardial infarction (STEMI) two years earlier and smoking (ALCOHOL?). Coronarography performed two years before admission had revealed 40% stenosis of the left anterior descending artery (LAD) with no other lesions. 

- How was history obtained if the patient was unconscious and was immediately referred to coro?
- Hypothesis of DCM due to alcohol?

Based on history (ONLY???), it was decided to perform coronarography, confirming the 40% stenosis of the LAD, which was deemed insignificant at the time. XXX therapy ensued.

However, during rehabilitation the patient unexpectedly reported angina, which had not been present/DENIED? in history. Having excluded other causes, another evaluation of the coronary arteries was performed. Surprisingly, a fractional flow reserve (FFR) of 0.77 indicated that the 40% stenosis was indeed significant. The next day two stents were implanted. 

- Why only the next day?
- What kind of stents? Is it important?

A day later, SCA caused by cardiac tamponade occurred. Emergency pericardiocentesis was performed but no bleeding source was found. Unexpectedly, the procedure had not resulted in improved contractility and decreased contractility persisted (LVEF of 20%). 

Restenosis was hypothesized and another coronarography was performed to confirm. Blood flow through the stents was unobstructed. Instead, an episode of a severe proximal spasm of the LAD was detected, subsiding after nitroglycerin injection. Calcium channel blocker was initiated for common variant angina. Further recovery was uneventful.

## Discussion

The case presented particular diagnostic and therapeutic challenges due to a coincidence of the following factors:

1. The presence of alcohol-related DCM.
2. Typical STEMI presentation in ECG on admission and in history.
3. A seemingly insignificant stenosis of 40% on admission and in history.
4. A significant FFR obstruction of 0.77 upon further investigation.
5. A sudden appearance of angina after coronarography and successful stent implantation.
6. A mysterious occurrence of cardiac tamponade. 
7. A serendipitous discovery of a spasm of the LAD. 

The exact causal nexus of events and the aetiology of this case of MINOCA is not entirely clear but several hypotheses can be raised.

It is likely that the primary cause of STEMI in this patient was the manifestly 40% stenosis, which may have in reality been larger but under appreciated by angiography — as it is known to occasionally show less atherosclerotic plaque than with other methods, such as Optical Coherence Tomography and/or IntraVascular UltraSound.(citation) It is quite possible that stent implantation had actually removed this cause. 

However, a subsequent appearance of angina may have been iatrogenic — as a result of spasms provoked by coronarography. This hypothesis is supported by the fact that angina had not been present in earlier history and was only observed after (and during) coronarography. 

The occurrence cardiac tamponade still remains a mystery.

## Conclusions

At the time of the case, the concept of MINOCA and, consequently, its diagnostic guidelines were nonexistent. Consequently, the case was serendipitously decided by a brief and probably rare vasospastic episode manifested during coronarography administered for and entirely different reason. 

As illustrated by this case, the new concept of MINOCA and the diagnostic test flow chart unveiled in the new Guidelines can be valuable tools for every practitioner.

Yet it is worth remembering that such challenging cases as they are, can be further complicated by unexpected an potentially lethal developments (such as cardiac tamponade in our case) and clouded by preexisting conditions and/or even iatrogenic factors (alcohol-related DCM and LAD spasm, respectively).